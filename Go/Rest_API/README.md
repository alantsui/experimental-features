### 1. Download link for Postman
```
https://www.getpostman.com/downloads/
```

### 2. Read Data:
1. Just type in the api links in POSTMAN and Send with appropriate Tags( Put,Delete,Post,etc)
### 3. Writing Data
1. In `headers`
```
Content-Type:application/json
```
2. In `body`
```sh
# Select raw
# Type in json String
# Send
```

Useful Links: https://www.saltycrane.com/blog/2019/01/how-run-postgresql-docker-mac-local-development/