# Debian setup after installation

1. ADD Sudo Users
```sh
su -
usermod -aG sudo $USER
exit
# logout
```
_First snapshort_
2. Upgrade to testing
```sh
sudo apt update
sudo apt upgrade
sudo bash -c 'echo "deb http://mirror.xtom.com.hk/debian/ testing main contrib non-free" > sources.list'
sudo apt update
sudo apt upgrade
sudo apt upgrade --fix-missing
sudo apt dist-upgrade
sudo apt autoremove
```

4. Install theme
```
Install sweet theme
Install mojave theme
Apply sweet with layout
Apply mojave theme
```
_Second Snapshot_

5. UEFI Bugs in virtualbox, debian
    5.1 method 1
```sh
fs0:
cd \efi
ls
#Check the name of folder entry
cd $DISTRONAME
#locate grubx64.efi
cd ../
cd ../
echo '\path\to\grubx64.efi' >> startup.nsh
#eg \efi\debian\grubx64.efi
reset
```

5.2 method 2
```sh
sudo -i
mkdir -p /boot/efi/EFI/boot
touch /boot/efi/EFI/boot/bootx64.efi
cp /boot/efi/EFI/grubx64.efi /boot/efi/EFI/boot/bootx64.efi
```

6. Install other software
```sh
sudo apt install snapd ffmpeg nodejs sqlite tree yarn smplayer software-properties-common python3-pip3 
```
7 Install repo software
```sh
wget -qO- https://repo.vivaldi.com/archive/linux_signing_key.pub | sudo apt-key add -
sudo add-apt-repository 'deb https://repo.vivaldi.com/archive/deb/ stable main'
sudo apt update && sudo apt install vivaldi-stable

```

8. pip3 install
```sh
sudo pip3 install pillow youtube_dl scipy scikit-image requests pylint opencv-python
```
