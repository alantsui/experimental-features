## Process management
1. [Fork and exec](https://www.youtube.com/watch?v=nwm7rJG90i8)
2. [Linux process management](https://www.tecmint.com/linux-process-management/)
3. [Wait](https://nanxiao.me/bash-shell-wait/)
4. [Process Scheduling](https://ithelp.ithome.com.tw/articles/10204690)


## Thread and concurrency
1. [Threading](https://jjstudy.weebly.com/multi-thread/linux-multi-thread)
2. [Concurrency management](https://hackmd.io/@VIRqdo35SvekIiH4p76B7g/Hy9JyrBw?type=view)
3. [Concurrency vs Parallelism](https://medium.com/mr-efacani-teatime/concurrency%E8%88%87parallelism%E7%9A%84%E4%B8%8D%E5%90%8C%E4%B9%8B%E8%99%95-1b212a020e30)

## Sockets
1. [TCP Socket Programming](http://zake7749.github.io/2015/03/17/SocketProgramming/)
2. [Golang Sockets](https://hiberabyss.github.io/2018/03/14/unix-socket-programming/)
3. [Rust Sockets](https://magiclen.org/rust-multi-thread-web-server/)
4. [Python Sockets](https://realpython.com/python-sockets/)

## I/O management
1. [Process I/O management](http://www.hasanbalik.com/LectureNotes/OpSys/Assignments/Linux%20(Fedora%20or%20Slackware)%20IO%20Management%20%20and%20Disk%20Scheduling.pdf)

## Memory management
1. [memory management](https://mropengate.blogspot.com/2015/01/operating-system-ch8-memory-management.html)
2. [memory management](https://ithelp.ithome.com.tw/articles/10208175)

## File System
1. [File system](https://ithelp.ithome.com.tw/articles/10209064)
2. [File System](https://mropengate.blogspot.com/2015/01/operating-system-ch11-file-system.html)
3. [File System implementation](https://mropengate.blogspot.com/2015/01/operating-system-ch11-file-system.html)