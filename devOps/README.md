# Guidelines on road to devOps

1. Learn 1 of 3 languages， my target:
- [ ] C
- [ ] Rust
- [ ] Python
- [x] Golang

2. Understand OS cencepts 
- [ ] Process Management
- [ ] Threads and concurrency
- [ ] Sockets
- [ ] I/O management
- [ ] Virtualization
- [ ] Memory storage
- [ ] File Systems

3. Linux command Refresh
- [ ] find
- [ ] grep
- [ ] awk
- [ ] sed
- [ ] lsof
- [ ] nslookup
- [ ] Axel

4. Networking and security
- [ ] DNS
- [ ] OSI Model
- [ ] HTTP
- [ ] HTTPS
- [ ] FTP
- [ ] SSL
- [ ] TLS

5. Setup
* servers
- [ ] Apache
- [ ] Tomcat
- [ ] nginx
- [ ] caching server
- [ ] Load balancer
- [ ] Reverse Proxy
- [ ] Firewall
![Infrastructures](https://hackernoon.com/hn-images/1*c7z6N5xH8t1i6msXzIuWHw.gif)

6. Infrastructures as code
- [ ] Containers: docker
- [ ] Container: Kubernates
Configuration management tool:
- [ ] Ansible
- [ ] Chef
- [ ] Salt
- [ ] Puppet

7. CI/CD Tools
- [ ] Jenkins
- [ ] Gitlab CI
- [ ] circle CI

8.monitoring software and infrastructure
- [ ] Nagios
- [ ] Icing
- [ ] Datadog
- [ ] Zabbix
- [ ] Monit
- [ ] AppDynanic
- [ ] New Relic

9.Cloud Providers
- [ ] AWS
- [ ] Google Cloud
- [ ] Azure


https://github.com/goodjack/developer-roadmap-chinese