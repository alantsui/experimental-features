# List of Linux skills

[The reference](https://www.udemy.com/linux-administration-bootcamp/?ranMID=39197&ranEAID=JVFxdTr9V80&ranSiteID=JVFxdTr9V80-65un7WJHxj_iJZhZWBN1bA&LSNPUBID=JVFxdTr9V80)

1. My environment

| OS | Debian 
------|------
| Version | Testing
| Desktop Environment | CMD

2. fundamentals
- [ ] Working with directories (cd)
- [ ] Listing files (ls)
- [ ] Understanding file and directory permissions
- [ ] finding files and directories
- [ ] viewing files(cat/bat) and the nano editor
- [ ] Editing files in vim editor
- [ ] Graphical editors
- [ ] Deleting, Copying, Moving and renaming files(mkdir -p path/to/dir)
3. Interediate skills
- [ ] wildcard
- [ ] comparing files
- [ ] input, output and redirections ( cat hello > world cat hello >> world)
- [ ] searching in files and using pipes
- [ ] transfer and copy files using network (curl, wget, axel, aria2c)
- [ ] customize shell prompt
- [ ] Process and job control
- [ ] Scheduling repeated jobs with cron
- [ ] switching users and run command as others ( su - $USER)
- [ ] Shell History and tab completion
- [ ] Installing software (apt)

4. Linux Boot process and system logging
- [ ] The linux boot process
- [ ] System Logging

5. Disk Management
- [ ] create partitions with gdisk(EFI) or fdisk(bios)
- [ ] file systems

6. LVM -- Logical Volumes manager
_Caution: LVM is not recommend if you are trying to install linux in only 1 drive_
- [ ] Layers of abstraction
- [ ] creating Physical Volume, Volume Group, Logical Volumes
- [ ] Extend Volume group and logical volumes
- [ ] mirroring logical volumes
- [ ] Removing Logical Volumes, Physical Volumes, and Volume Groups
- [ ] Migrating Data from One Storage Device to Another

7. Networking
- [ ] TCP/IP networking for Linux System administrators
- [ ] DNS and hostname
- [ ] DHCP, Dynamic and Static Addressing
- [ ] Network Troubleshooting -> (ref cisco)

8. Special Permission Modes
- [ ] Special Permission Modes

9. Other things
- [ ] Install LAMP stack in ubuntu




