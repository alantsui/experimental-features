# Nginx
* [Nginx basic](https://blog.hellojcc.tw/2015/12/07/nginx-beginner-tutorial/)
* [nginx docker](https://blog.techbridge.cc/2018/03/17/docker-build-nginx-tutorial/)
* [nginx config generator](https://nginxconfig.io/?0.php=false&0.index=index.html&0.fallback_html&ssl_profile=modern)

# reverse proxy
* [nginx reverse proxy](https://ithelp.ithome.com.tw/articles/10188498)
* [reverse proxy](https://www.zhihu.com/question/24723688)

# Linux server networking
* [birdy](http://linux.vbird.org/linux_server/0107cloudandvm.php)

# Load balancer
* [nginx load balancer setting](https://blog.dtask.idv.tw/Nginx/2018-07-31/)

# Cache server
* [nginx cache server](https://tomme.me/nginx-proxy-cache-server/)

# Firewall
* [CentOS](https://www.jinnsblog.com/2018/08/centos-firewalld-guide.html)
* [Linux](https://stackoverflow.max-everyday.com/2017/09/linux-firewall-iptables/)
* [CentOS](https://blog.gtwang.org/linux/centos-7-firewalld-command-setup-tutorial/)