# Infrastructure as code

1. Concepts of devOps
- [鐵人賽](https://ithelp.ithome.com.tw/articles/10199565)

2. Docker
- [docker](https://ithelp.ithome.com.tw/articles/10190614)
- [docker compose](https://ithelp.ithome.com.tw/users/20072603/ironman/2088)

3. Kubernates
- [docker & kubernates](https://ithelp.ithome.com.tw/users/20072603/ironman/2088)
- [kubernates](https://ithelp.ithome.com.tw/users/20103753/ironman/1590)

4. Ansible
- [30 天鐵人賽](https://ithelp.ithome.com.tw/articles/10191403)

5. Chef
- [basics](https://medium.com/@chihsuan/chef-自動化部署工具-74ab1f7065e4)
- [Advanced chef](https://medium.com/@luyo)

6. saltstack
- [features](http://www.voidcn.com/article/p-vvasznto-bnz.html)
- [basics](https://blog.littlero.se/post/nice-to-meet-saltstack/)
7. Puppet
- [鐵人賽](https://ithelp.ithome.com.tw/users/20089211/ironman/1262)


