# CI/CD tools

1. Overview on different CI tools
- [鐵人賽](https://ithelp.ithome.com.tw/users/20102562/ironman/987?page=3)
2. jenkins
- [30天鐵人賽](https://ithelp.ithome.com.tw/articles/10193903)
3. Circle CI
- [Concept + basics](https://medium.com/@paulchen_9650/%E8%B6%85%E6%96%B0%E6%89%8B%E7%9A%84%E4%B8%80%E6%97%A5ci-cd%E5%88%9D%E9%AB%94%E9%A9%97-%E4%BD%BF%E7%94%A8circleci-github-flow%E8%87%AA%E5%8B%95%E9%83%A8%E7%BD%B2node-js%E6%96%BCaws-elastic-beanstalk-e7af3a65ae61)
- [basics](https://medium.com/@evenchange4/使用-circleci-2-0-workflows-挑戰三倍速-9691e54b0ef0)

4. gitlab CI
- [basics](https://kheresy.wordpress.com/2019/02/13/gitlab-ci-cd/)
- [with c++](https://kheresy.wordpress.com/2019/07/25/c-project-script-for-gitlab-ci-with-powershell/)
- [with c++ 2](https://kheresy.wordpress.com/2019/07/10/gitlab-c-project-ci-script/)
- [advanced, build gitlab CE](https://kheresy.wordpress.com/2019/01/08/build-gitlab-server/ s)
- [basics2](https://greddywork.gitlab.io/greddyblogs/archives/page/3/)

