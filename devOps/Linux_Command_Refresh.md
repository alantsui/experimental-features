# Find
1. [Find tuts](https://blog.gtwang.org/linux/unix-linux-find-command-examples/)

# awk, grep sed
1. [awk, grep, sed](https://thief.one/2017/08/12/1/)

# lsof
1. [lsof](https://blog.gtwang.org/linux/linux-lsof-command-list-open-files-tutorial-examples/)

# nslookup
1. [nslookup windows](http://blog.qoding.us/2011/02/how-to-use-nslookup-to-verify-dns-configuration/)
2. [nslookup完整教學](https://blog.xuite.net/leocat999/aming/214503310-WHOIS%2CDNS%2Cnslookup%E7%9A%84%E6%8C%87%E4%BB%A4)

# Axel
1. [Axel](https://man.linuxde.net/axel)